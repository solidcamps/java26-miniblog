package pl.sda.java.miniblog26.hello;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service
@Qualifier("constantTemp")
public class ConstantTemperatureService implements TemperatureProvider {

    @Override
    public Integer currentTemperature() {
        return 23;
    }
}
