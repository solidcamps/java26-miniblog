package pl.sda.java.miniblog26.hello;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;

@Scope("prototype")
@Service
public class CacheService<T> {

    private final HelloDateTimeProvider helloDateTimeProvider;

//    private Object cachedObject;
    private T cachedObject;
    private LocalDateTime expireTime;

    public CacheService(HelloDateTimeProvider helloDateTimeProvider) {
        this.helloDateTimeProvider = helloDateTimeProvider;
    }

    //    public Object get() {
//    public Optional<T> get() {
    public T get() {
        if (expireTime != null && helloDateTimeProvider.getCurrentTime().isAfter(expireTime)) {
            return null;
        }
        return cachedObject;
    }

//    public void put(Object object) {
    public void put(T object, Duration duration) {
        expireTime = helloDateTimeProvider.getCurrentTime().plus(duration);
        cachedObject = object;
    }

}
