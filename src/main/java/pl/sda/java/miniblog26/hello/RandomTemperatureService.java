package pl.sda.java.miniblog26.hello;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Random;

@Primary
@Service
public class RandomTemperatureService implements TemperatureProvider {

    private final CacheService<Integer> cacheService;

    public RandomTemperatureService(CacheService<Integer> cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    public Integer currentTemperature() {
        final Integer cachedTemperature = cacheService.get();
        if (cachedTemperature != null) {
            return cachedTemperature;
        }

        final Random random = new Random();
        final int randomTemp = random.nextInt(31);

        cacheService.put(randomTemp, Duration.ofSeconds(5));

        return randomTemp;
    }
}
