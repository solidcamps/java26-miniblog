package pl.sda.java.miniblog26.hello;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class StringStatisticsService {
    public int sumAllCharacters(String... inputStrings) {

//        int countedChars = 0;
//        for (String inputString : inputStrings) {
////            countedChars = countedChars + inputString.length();
//
////            if (inputString != null) {
////                countedChars += inputString.length();
////            }
//
////            countedChars += Optional.ofNullable(inputString)
//////                    .map(s -> s.length())
////                    .map(String::length)
////                    .orElse(0);
//
//            if (inputString == null) {
//                continue;
//            }
//            countedChars += inputString.length();
//        }
//        return countedChars;

        return Arrays.stream(inputStrings)
//                .filter(s -> s != null)
                .filter(Objects::nonNull)
                .mapToInt(String::length)
                .sum();
    }

    public boolean isLengthEven(String... inputStrings) {
        return sumAllCharacters(inputStrings) % 2 == 0;
    }

    public Map<String, Integer> countCharactersOccurrence(String... inputStrings) {

//        "ala ma kota".chars();
//        "ala ma kota".split("");

//        Map<String, Integer> charactersCountMap = new HashMap<>();
//        for (String inputString : inputStrings) {
//            if (inputString == null) {
//                continue;
//            }
//            final char[] chars = inputString.toCharArray();
//            for (char ch : chars) {
//                final String s = Character.toString(ch);
////                final Integer integer = charactersCountMap.get(s);
////                if (integer == null) {
////                    charactersCountMap.put(s, 1);
////                } else {
////                    charactersCountMap.put(s, integer + 1);
////                }
//                Integer currentCount = charactersCountMap.getOrDefault(s, 0);
//                charactersCountMap.put(s, currentCount + 1);
//            }
//        }
//        return charactersCountMap;

        return Arrays.stream(inputStrings)
                .filter(Objects::nonNull)
//                .flatMapToInt(s -> s.chars())
                .flatMapToInt(String::chars)
//                .mapToObj(i -> Integer.valueOf(i))
//                .mapToObj(Integer::valueOf)
                .boxed()
//                .collect(Collectors.groupingBy(c -> Character.toString(c), Collectors.counting()));
                .collect(Collectors.toMap(Character::toString, c -> 1, (existingInt, newInt) -> existingInt+newInt));
    }
}
