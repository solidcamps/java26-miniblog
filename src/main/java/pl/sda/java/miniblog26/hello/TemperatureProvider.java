package pl.sda.java.miniblog26.hello;

public interface TemperatureProvider {

    Integer currentTemperature();
}
