package pl.sda.java.miniblog26.hello;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class HelloDateTimeProvider {

    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }
}
