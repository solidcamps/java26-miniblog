package pl.sda.java.miniblog26.hello;

import lombok.Getter;
import lombok.Setter;

import java.util.StringJoiner;

@Getter
@Setter
public class HelloUser {
    private String firstname;
    private String lastname;
    private String password;

    public HelloUser() {
    }

    public HelloUser(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        System.out.println("Used getter method: getFirstname()");
        return firstname;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", HelloUser.class.getSimpleName() + "[", "]")
                .add("firstname='" + firstname + "'")
                .add("lastname='" + lastname + "'")
                .toString();
    }
}
