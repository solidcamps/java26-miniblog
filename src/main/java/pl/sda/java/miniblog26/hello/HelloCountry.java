package pl.sda.java.miniblog26.hello;

import lombok.Data;

@Data
public class HelloCountry {
    private final String name;
    private final Long area;
}
