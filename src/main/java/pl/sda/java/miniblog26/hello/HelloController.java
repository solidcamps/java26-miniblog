package pl.sda.java.miniblog26.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

//@Component
@Controller
public class HelloController {

    private final TemperatureProvider temperatureService;
    private final SayingsService sayingsService;
    private final StringStatisticsService stringStatisticsService;

    @Autowired //optional
    public HelloController(/*@Qualifier("constantTemp") */TemperatureProvider temperatureService,
                                                          SayingsService sayingsService,
                                                          StringStatisticsService stringStatisticsService) {
        this.temperatureService = temperatureService;
        this.sayingsService = sayingsService;
        this.stringStatisticsService = stringStatisticsService;
    }

    @GetMapping("/hello")
    public String helloPage() {

        return "hello/helloView";
    }

    @GetMapping("/hello-user")
    public String helloUserPage(@RequestParam(name = "name") String nameParam,
                                Model model) {
        //Map.put("username", name)
        model.addAttribute("username", nameParam);
//        System.out.println(name);
        return "hello/helloDynamicUserView";
    }


    @GetMapping("/hello-{name}")
    public String helloUserPageByPath(@PathVariable String name,
                                Model model) {
        //Map.put("username", name)
        model.addAttribute("username", name);
//        System.out.println(name);
        return "hello/helloDynamicUserView";
    }

    @GetMapping("/hello-user-object")
    public String helloUserPageWithObject(@RequestParam String firstname,
                                          @RequestParam(required = false) String lastname,
                                Model model) {
//        model.addAttribute("firstname", firstname);
//        model.addAttribute("lastname", lastname);
        final HelloUser user = new HelloUser(firstname, lastname);
        model.addAttribute("user", user);

        final List<HelloCountry> helloCountries = List.of(
                new HelloCountry("Poland", 100L),
                new HelloCountry("USA", 10000L));

//        final ArrayList<HelloCountry> helloCountryArrayList = new ArrayList<>();
//        helloCountryArrayList.add(new HelloCountry("Poland", 100L));
//        helloCountryArrayList.add(new HelloCountry("USA", 10000L));

        model.addAttribute("countries", helloCountries);

        return "hello/helloUserObjectView";
    }

    @GetMapping("/hello-user-form")
    public String helloUserPageForm(Model model) {
        final HelloUser user = new HelloUser();
        model.addAttribute("user", user);
        return "hello/helloUserFormView";
    }

    @GetMapping("/hello-user-form-submit")
    public String helloUserSubmitForm(@RequestParam String firstname,
                                      @RequestParam String lastname,
                                      Model model) {

        final HelloUser user = new HelloUser(firstname, lastname);
        model.addAttribute("user", user);

        return "hello/helloUserAfterFormSubmit";
    }

    @GetMapping("/hello-user-form-submit-2")
    public String helloUserSubmitForm2(@RequestParam String firstname,
                                      @RequestParam String lastname,
                                      Model model) {

        final HelloUser user = new HelloUser(firstname, lastname);
        model.addAttribute("user", user);

        return "hello/helloUserFormView";
    }

    //TODO: stworzyć widok pod ścieżką /info pokazujący bieżącą temperaturę.
    @GetMapping("/info")
    public String showInformationPage(Model model) {
        model.addAttribute("currentTemp", temperatureService.currentTemperature());
        model.addAttribute("randomSaying", sayingsService.getRandomSaying());
        return "hello/informationView";
    }

    @GetMapping("/hello-words")
    public String showWordsForm() {
        return "hello/helloWords";
    }

    @GetMapping("/hello-words-submit")
    public String showWordsFormSubmit(@RequestParam(name = "word1") String firstWord,
                                      @RequestParam(name = "word2") String secondWord,
                                      Model model) {

        int lettersCount = stringStatisticsService.sumAllCharacters(firstWord, secondWord);
        boolean lengthIsEven = stringStatisticsService.isLengthEven(firstWord, secondWord);
        Map<String, Integer> charactersCountMap = stringStatisticsService.countCharactersOccurrence(firstWord, secondWord);

        return "hello/helloWords";
    }

}
