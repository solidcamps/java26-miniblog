package pl.sda.java.miniblog26.api;

import java.util.StringJoiner;

public class PostApiDto {
    private String title;
    private String body;

    public PostApiDto(String title, String body) {
        this.title = title;
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PostApiDto.class.getSimpleName() + "[", "]")
                .add("title='" + title + "'")
                .add("body='" + body + "'")
                .toString();
    }
}
