package pl.sda.java.miniblog26.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api")
public class PostRestController {

//    @ResponseBody
    @GetMapping("/posts")
    List<PostApiDto> getAllPosts(@RequestParam(required = false) String q,
                                 @RequestHeader("API-KEY") String apiKey) {
        if (!"secret".equals(apiKey)) {
            throw new RuntimeException();
        }
        log.info("Param q: {}", q);
        return List.of(new PostApiDto("test title", "test body"));
    }
}
