package pl.sda.java.miniblog26.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.java.miniblog26.entities.PostEntity;

public interface PostRepository extends JpaRepository<PostEntity, Long> {
}
