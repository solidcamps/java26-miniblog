package pl.sda.java.miniblog26.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.java.miniblog26.entities.PostCommentEntity;

import java.util.List;

public interface PostCommentRepository extends JpaRepository<PostCommentEntity, Long> {
    List<PostCommentEntity> findByPostId(Long postId, Sort sort);
}
