package pl.sda.java.miniblog26.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.sda.java.miniblog26.entities.UserEntity;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    List<UserEntity> findByEmail(String xyz);

    @Query("SELECT u FROM UserEntity u WHERE u.email = ?1")
    Optional<UserEntity> findUserByLogin(String email);

    List<UserEntity> findByEmailContaining(String emailLike);

    List<UserEntity> findByEmailContaining(String emailLike, Sort sort);

    List<UserEntity> findByEmailContainingOrderByNickname(String emailLike);

    List<UserEntity> findByEmailAndNickname(String email, String nickname);
}
