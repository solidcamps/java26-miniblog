package pl.sda.java.miniblog26.dao;

import org.springframework.stereotype.Repository;
import pl.sda.java.miniblog26.entities.UserEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    void save(UserEntity userEntity) {
        //begin

        entityManager.persist(userEntity);

        //commit
    }
}
