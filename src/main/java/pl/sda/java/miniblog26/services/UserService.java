package pl.sda.java.miniblog26.services;

import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.java.miniblog26.dao.UserDao;
import pl.sda.java.miniblog26.dao.UserRepository;
import pl.sda.java.miniblog26.dtos.RegisteredUserDto;
import pl.sda.java.miniblog26.dtos.UserDetailsDto;
import pl.sda.java.miniblog26.dtos.UserShortInfoDto;
import pl.sda.java.miniblog26.entities.UserEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserDao userDao;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    //    @Autowired
    public UserService(UserDao userDao, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public RegisteredUserDto registerUser(String email, String nickname, String password) {
        final UserEntity userEntity = new UserEntity();
        userEntity.setEmail(email);
        userEntity.setNickname(nickname);
        userEntity.setPassword(passwordEncoder.encode(password));

//        userDao.save(userEntity);
        userRepository.save(userEntity);

        return null;
    }

    public List<UserShortInfoDto> getAllUsers() {

//        final Iterable<UserEntity> userEntities = userRepository.findAll();
//        List<UserShortInfoDto> userDtos = new ArrayList<>();
//        for (UserEntity userEntity : userEntities) {
//            var dto = new UserShortInfoDto(userEntity.getId(), userEntity.getEmail(), userEntity.getNickname());
//            userDtos.add(dto);
//        }
//        return userDtos;

        return userRepository.findAll().stream()
                .map(userEntity -> new UserShortInfoDto(userEntity.getId(), userEntity.getEmail(), userEntity.getNickname()))
                .collect(Collectors.toList());

    }

    public List<UserShortInfoDto> getUsersByEmail(String email) {
        return userRepository
//                .findByEmailContaining(email)
//                .findByEmailContainingOrderByNickname(email)
                .findByEmailContaining(email, Sort.by("email").descending())
                .stream()
                .map(userEntity -> new UserShortInfoDto(userEntity.getId(), userEntity.getEmail(), userEntity.getNickname()))
                .collect(Collectors.toList());
    }

    public Optional<UserDetailsDto> getUserDetails(Long userId) {
//    public UserDetailsDto getUserDetails(Long userId) {
        return userRepository.findById(userId)
                .map(entity -> new UserDetailsDto(entity.getId(), entity.getEmail(), entity.getNickname(), entity.getCreated()));

//        return userRepository.findById(userId)
//                .map(entity -> new UserDetailsDto(entity.getId(), entity.getEmail(), entity.getNickname(), entity.getCreated()))
//                .orElseThrow(() -> new IllegalArgumentException("User doesn't exist for given id: " + userId));

    }

    public UserDetailsDto getUserDetailsWithException(Long userId) {
        return userRepository.findById(userId)
                .map(entity -> new UserDetailsDto(entity.getId(), entity.getEmail(), entity.getNickname(), entity.getCreated()))
                .orElseThrow(() -> new IllegalArgumentException("User doesn't exist for given id: " + userId));

    }

}
