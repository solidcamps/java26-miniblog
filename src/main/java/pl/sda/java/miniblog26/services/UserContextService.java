package pl.sda.java.miniblog26.services;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
//@SessionScope
public class UserContextService {

    public String getCurrentlyLoggedUsername() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            return null;
        }
//        authentication.getAuthorities()
        return authentication.getName();
    }
}
