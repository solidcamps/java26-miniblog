package pl.sda.java.miniblog26.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import pl.sda.java.miniblog26.dao.UserRepository;
import pl.sda.java.miniblog26.dtos.UserSessionDto;
import pl.sda.java.miniblog26.entities.UserEntity;
import pl.sda.java.miniblog26.exceptions.InvalidCredentialsException;
import pl.sda.java.miniblog26.exceptions.UserDoesntExistException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.UUID;

@Slf4j
@Service
//@Scope("prototype")
@SessionScope
public class LoginService implements InitializingBean {

    private final UserRepository userRepository;

    private UUID uuid;

    private boolean logged;
    private UserSessionDto userSessionDto;

    public LoginService(UserRepository userRepository) {
        uuid = UUID.randomUUID();
        logged = false;
        log.info("LoginService creating instance: {}", uuid.toString());
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void init() {
        log.info("LoginService @PostConstruct: {}", uuid.toString());
    }

    @PreDestroy
    public void destroy() {
        log.info("LoginService @PreDestroy: {}", uuid.toString());
    }

    public void loginUser(String username, String password) {
        log.info("LoginService loginUser: {} / {}", username, uuid.toString());
//        final List<UserEntity> foundUsers = userRepository.findByEmail(username);
//        if (foundUsers.isEmpty()) {
//            throw new UserDoesntExistException(username);
//        }
//        if (foundUsers.size() > 1) {
//            //...
//        }
//        final UserEntity userEntity = foundUsers.get(0);

        final UserEntity userEntity = userRepository.findUserByLogin(username)
                .orElseThrow(() -> new UserDoesntExistException(username));

        if (!userEntity.getPassword().equals(password)) {
            throw new InvalidCredentialsException("invalid password");
        }

        this.userSessionDto = new UserSessionDto(userEntity.getEmail(), userEntity.getNickname());
        this.logged = true;
    }

    public boolean isLogged() {
        return logged;
    }

    public UserSessionDto getUserSessionDto() {
        return userSessionDto;
    }

    public void logout() {
        this.logged = false;
        this.userSessionDto = null;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("LoginService afterPropertiesSet: {}", uuid.toString());

    }
}
