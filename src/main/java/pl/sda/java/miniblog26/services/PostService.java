package pl.sda.java.miniblog26.services;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.sda.java.miniblog26.dao.PostCommentRepository;
import pl.sda.java.miniblog26.dao.PostRepository;
import pl.sda.java.miniblog26.dtos.CommentDto;
import pl.sda.java.miniblog26.dtos.PostInfoDto;
import pl.sda.java.miniblog26.dtos.PostShortInfoDto;
import pl.sda.java.miniblog26.dtos.request.NewPostForm;
import pl.sda.java.miniblog26.entities.PostCommentEntity;
import pl.sda.java.miniblog26.entities.PostEntity;
import pl.sda.java.miniblog26.exceptions.PostNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostService {

    private final PostRepository postRepository;
    private final PostCommentRepository postCommentRepository;

    public PostService(PostRepository postRepository, PostCommentRepository postCommentRepository) {
        this.postRepository = postRepository;
        this.postCommentRepository = postCommentRepository;
    }

    public void saveNewPost(NewPostForm newPostForm) {

        final PostEntity postEntity = new PostEntity();
        postEntity.setTitle(newPostForm.getPostTitle());
        postEntity.setBody(newPostForm.getPostBody());

        postRepository.save(postEntity);
    }

    public List<PostShortInfoDto> getAllPostsSortedByNewest() {

        return postRepository.findAll(Sort.by("added").descending())
                .stream()
                .map(postEntity -> new PostShortInfoDto(postEntity.getId(),
                        postEntity.getTitle(),
                        postEntity.getBody(),
                        postEntity.getAdded()))
                .collect(Collectors.toList());

    }

    public Optional<PostInfoDto> getSinglePostInfo(Long postId) {
        return postRepository.findById(postId)
                .map(postEntity -> new PostInfoDto(postEntity.getId(),
                        postEntity.getTitle(),
                        postEntity.getBody(),
                        postEntity.getAdded()));
    }

    public void addNewComment(Long postId, String commentText, String nickname) {

        final PostEntity postEntity = postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException(postId));

        final PostCommentEntity postCommentEntity = new PostCommentEntity();
        postCommentEntity.setCommentText(commentText);
        postCommentEntity.setCommentatorNickname(nickname);
        postCommentEntity.setPost(postEntity);

        postCommentRepository.save(postCommentEntity);
    }

    public Optional<PostInfoDto> getSinglePostInfoWithComments(Long postId) {
        final Optional<PostInfoDto> postInfoDtoOptional = postRepository.findById(postId)
                .map(postEntity -> new PostInfoDto(postEntity.getId(),
                        postEntity.getTitle(),
                        postEntity.getBody(),
                        postEntity.getAdded()));

        postInfoDtoOptional.ifPresent(postInfoDto -> {
            final List<CommentDto> commentDtos = postCommentRepository
                    .findByPostId(postId, Sort.by("added").descending())
                    .stream()
                    .map(postCommentEntity -> new CommentDto(postCommentEntity.getCommentatorNickname(),
                            postCommentEntity.getAdded(),
                            postCommentEntity.getCommentText()))
                    .collect(Collectors.toList());

            postInfoDto.setComments(commentDtos);
        });

        return postInfoDtoOptional;
    }
}
