package pl.sda.java.miniblog26.dtos;

import java.time.LocalDateTime;

public class CommentDto {
    private String nickname;
    private LocalDateTime added;
    private String text;

    public CommentDto(String nickname, LocalDateTime added, String text) {
        this.nickname = nickname;
        this.added = added;
        this.text = text;
    }

    public String getNickname() {
        return nickname;
    }

    public LocalDateTime getAdded() {
        return added;
    }

    public String getText() {
        return text;
    }
}
