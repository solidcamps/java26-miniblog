package pl.sda.java.miniblog26.dtos;

import java.time.LocalDateTime;
import java.util.List;

public class PostInfoDto {
    private Long id;
    private String title;
    private String body;
    private LocalDateTime added;
    private List<CommentDto> comments;

    public PostInfoDto(Long id, String title, String body, LocalDateTime added) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.added = added;
    }

    public void setComments(List<CommentDto> comments) {
        this.comments = comments;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public LocalDateTime getAdded() {
        return added;
    }

    public List<CommentDto> getComments() {
        return comments;
    }
}
