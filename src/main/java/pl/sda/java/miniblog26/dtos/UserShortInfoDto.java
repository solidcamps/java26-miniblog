package pl.sda.java.miniblog26.dtos;

public class UserShortInfoDto {
    private Long id;
    private String email;
    private String nickname;

    public UserShortInfoDto(Long id, String email, String nickname) {
        this.id = id;
        this.email = email;
        this.nickname = nickname;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getNickname() {
        return nickname;
    }
}
