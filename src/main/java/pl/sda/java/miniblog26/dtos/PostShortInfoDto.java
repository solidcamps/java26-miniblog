package pl.sda.java.miniblog26.dtos;

import java.time.LocalDateTime;

public class PostShortInfoDto {
    private Long id;
    private String title;
    private String bodyAbstract;
    private LocalDateTime added;

    public PostShortInfoDto(Long id, String title, String bodyAbstract, LocalDateTime added) {
        this.id = id;
        this.title = title;
        this.bodyAbstract = bodyAbstract;
        this.added = added;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBodyAbstract() {
        return bodyAbstract;
    }

    public LocalDateTime getAdded() {
        return added;
    }
}
