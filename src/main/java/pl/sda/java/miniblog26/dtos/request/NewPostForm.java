package pl.sda.java.miniblog26.dtos.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.StringJoiner;

public class NewPostForm {

    @Size(min = 3, max = 100)
    @NotBlank(message = "Tytuł nie może być pusty, ani zawierać samych białych znaków")
    private String postTitle;

    @NotNull
    @Size(max = 2000)
    private String postBody;

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", NewPostForm.class.getSimpleName() + "[", "]")
                .add("postTitle='" + postTitle + "'")
                .add("postBody='" + postBody + "'")
                .toString();
    }
}
