package pl.sda.java.miniblog26.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "post_comments")
public class PostCommentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String commentText;

    private String commentatorNickname;

    private LocalDateTime added = LocalDateTime.now();

    @ManyToOne
    @JoinColumn(name = "post_id")
    private PostEntity post;

    public PostEntity getPost() {
        return post;
    }

    public Long getId() {
        return id;
    }

    public String getCommentText() {
        return commentText;
    }

    public String getCommentatorNickname() {
        return commentatorNickname;
    }

    public LocalDateTime getAdded() {
        return added;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public void setCommentatorNickname(String commentatorNickname) {
        this.commentatorNickname = commentatorNickname;
    }

    public void setAdded(LocalDateTime added) {
        this.added = added;
    }

    public void setPost(PostEntity post) {
        this.post = post;
    }
}
