package pl.sda.java.miniblog26.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.sda.java.miniblog26.services.LoginService;
import pl.sda.java.miniblog26.services.UserContextService;

@Controller
public class HomePageController {

    private final LoginService loginService;
    private final UserContextService userContextService;

    public HomePageController(LoginService loginService, UserContextService userContextService) {
        this.loginService = loginService;
        this.userContextService = userContextService;
    }

    @GetMapping("/")
    public String showHomePage(Model model) {
        model.addAttribute("userLogged", loginService.isLogged());
        model.addAttribute("userInfo", loginService.getUserSessionDto());
        model.addAttribute("loggedAs", userContextService.getCurrentlyLoggedUsername());
        return "homePage";
    }
}
