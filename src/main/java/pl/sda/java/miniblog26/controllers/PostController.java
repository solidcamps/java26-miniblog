package pl.sda.java.miniblog26.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.java.miniblog26.dtos.PostInfoDto;
import pl.sda.java.miniblog26.dtos.request.NewPostForm;
import pl.sda.java.miniblog26.exceptions.PostNotFoundException;
import pl.sda.java.miniblog26.services.PostService;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

@Slf4j
@Controller
public class PostController {

    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping("/posts/add")
    public String showNewPostForm(Model model) {

        final NewPostForm newPostForm = new NewPostForm();
        newPostForm.setPostTitle("New Title");
        newPostForm.setPostBody("lorem ipsum...");
        model.addAttribute("newPostForm", newPostForm);

        return "posts/newPostForm";
    }

    @PostMapping("/posts/add")
    public String handleNewPostForm(@ModelAttribute @Valid NewPostForm newPostForm,
                                    BindingResult bindingResult) {

        log.info("New POST: {}", newPostForm);
        log.info("New POST ERRORS: {}", bindingResult.getAllErrors());

//        if (newPostForm.getPostTitle() == null || newPostForm.getPostTitle().isBlank()
//                || newPostForm.getPostTitle().length() < 5) {
//            throw new IllegalArgumentException("invalid user input for title");
//        }

        if (bindingResult.hasErrors()) {
            return "posts/newPostForm";
        }

        postService.saveNewPost(newPostForm);

        return "redirect:/";

    }

    @GetMapping("/posts")
    public String showAllPostsPage(Model model) {

        model.addAttribute("posts", postService.getAllPostsSortedByNewest());

        return "posts/postsView";
    }

    @GetMapping("/posts/{postId}")
    public String showSinglePostPage(@PathVariable Long postId, Model model) {

//        final Optional<PostInfoDto> postInfoDtoOptional = postService.getSinglePostInfo(postId);
        final Optional<PostInfoDto> postInfoDtoOptional = postService.getSinglePostInfoWithComments(postId);

        if (postInfoDtoOptional.isEmpty()) {
            return "posts/noPostFound";
        }

        model.addAttribute("post", postInfoDtoOptional.get());

//        List<CommentDto> postComments = postService.getCommentsForPost(postId);
//        model.addAttribute("postComments", postComments);

        return "posts/singlePostView";
    }

    @PostMapping("/posts/{postId}/comment/add")
    public String handleNewCommentForm(@PathVariable Long postId,
                                       @RequestParam String nickname,
                                       @RequestParam String commentText,
                                       RedirectAttributes redirectAttributes) {
        log.info("Comment for post: {}, nickname: {}, text: {}", postId, nickname, commentText);

        try {
            postService.addNewComment(postId, commentText, nickname);
        } catch (PostNotFoundException e) {
            return "posts/noPostFound";
        }

        redirectAttributes.addFlashAttribute("msg", "Comment added");
        return "redirect:/posts/" + postId;
    }

    @PostMapping("/post-image")
    public String addFile(@RequestParam MultipartFile file) throws IOException {
        final InputStream inputStream = file.getInputStream();
        final Path targetFile = Files.createFile(Path.of("c:/test"));
        Files.copy(inputStream, targetFile);

        return "";
    }

}
