package pl.sda.java.miniblog26.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import pl.sda.java.miniblog26.api.PostApiDto;

import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/ext")
public class PostExternalController {

    private final RestTemplate restTemplate;

    public PostExternalController(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @GetMapping("/posts")
    String getPostsFromAPI() {

        List<PostApiDto> posts = restTemplate.getForObject(
                "http://localhost:9090/api/posts?q=ala",
                List.class);

        HttpEntity entity = new HttpEntity(Map.of());
        final ResponseEntity<PostApiDto[]> exchange = restTemplate
                .exchange("http://localhost:9090/api/posts?q=ala",
                        HttpMethod.GET,
                        entity,
                        PostApiDto[].class);

        // OpenFeign
        // Retrofit

        //HttpClient - JDK 11+
        //HttpClient - ApacheCommons

        log.info("Posts: {}", posts);

        return null;
    }
}
