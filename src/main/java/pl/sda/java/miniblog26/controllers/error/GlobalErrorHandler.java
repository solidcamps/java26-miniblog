package pl.sda.java.miniblog26.controllers.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.sda.java.miniblog26.exceptions.PostNotFoundException;
import pl.sda.java.miniblog26.exceptions.UserDoesntExistException;

@Slf4j
@ControllerAdvice
public class GlobalErrorHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(UserDoesntExistException.class)
    public String handle(UserDoesntExistException e) {
        log.warn("Global exception handling for: {}", e.getMessage());

        return "posts/noPostFound";
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public String handle(IllegalArgumentException e) {
        log.warn("Global exception handling for: {}", e.getMessage());

        return "posts/noPostFound";
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(PostNotFoundException.class)
    public String handle(PostNotFoundException e) {
        log.warn("Couldn't find post with id: {}", e.getPostId());
        return "posts/noPostFound";
    }

    @ExceptionHandler(Exception.class)
    public String handle(Exception e) {
        log.error("Unknown exception: {}", e.getMessage());

        return "redirect:/";
    }
}
