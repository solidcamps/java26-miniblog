package pl.sda.java.miniblog26.controllers;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.sda.java.miniblog26.services.LoginService;

@Controller
public class LoginController {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(LoginController.class);

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @GetMapping("/login")
//    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginForm() {

        return "loginForm";
    }

    @PostMapping("/login-submit-data")
    public String submitLoginForm(@RequestParam String username,
                                  @RequestParam String password){
        try {
            loginService.loginUser(username, password);
        } catch (Exception e) {
            log.warn("Couldn't log user in: {}", e.getMessage(), e);
            return "redirect:/login";
        }

        return "redirect:/users-by-email?email=tt";
    }

    @GetMapping("/logout-user")
    public String logoutUser() {
        loginService.logout();
        return "redirect:/";
    }

    @GetMapping("/login-by-spring")
    public String showSpringLoginForm() {

        return "loginSpringForm";
    }


}
