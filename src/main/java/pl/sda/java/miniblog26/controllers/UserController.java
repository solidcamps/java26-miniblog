package pl.sda.java.miniblog26.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.sda.java.miniblog26.dtos.RegisteredUserDto;
import pl.sda.java.miniblog26.dtos.UserDetailsDto;
import pl.sda.java.miniblog26.dtos.UserShortInfoDto;
import pl.sda.java.miniblog26.exceptions.UserDoesntExistException;
import pl.sda.java.miniblog26.services.LoginService;
import pl.sda.java.miniblog26.services.UserService;

import java.util.List;

@Controller
public class UserController {

    private final UserService userService;
    private final LoginService loginService;

    public UserController(UserService userService, LoginService loginService) {
        this.userService = userService;
        this.loginService = loginService;
    }

    @GetMapping("/register")
    public String showRegisterForm() {
        return "registerForm";
    }

    @PostMapping("/register")
    public String submitRegisterForm(@RequestParam String email,
                                     @RequestParam String nickname,
                                     @RequestParam String password) {

        RegisteredUserDto registeredUserDto = userService.registerUser(email, nickname, password);
        return "registeredUserThankyouPage";
    }

    @GetMapping("/users")
    public String showUsersPage(Model model) {

        List<UserShortInfoDto> users = userService.getAllUsers();
        model.addAttribute("users", users);

        return "usersList";
    }

    @GetMapping("/users-by-email")
    public String showUsersByEmailPage(Model model, @RequestParam String email) {

        List<UserShortInfoDto> users = userService.getUsersByEmail(email);
        model.addAttribute("users", users);

        return "usersList";
    }

    // TODO:
    // Utwórz stronę pojedynczego użytkownika, pokazującą wszystkie jego dane razem z czasem utworzenia,
    // ale bez hasła. Strona ze szczegółami uzytkownika powinna być widoczna pod adresem typu: /users/1,
    // gdzie 1 to przykładowe id użytkownika.

    @GetMapping("/users/{userId}")
    public String showUserDetailsPage(@PathVariable Long userId, Model model) {

//        UserDetailsDto userDetails = userService.getUserDetails(userId);
        UserDetailsDto userDetails = userService.getUserDetails(userId)
//                .orElseThrow(() -> new IllegalArgumentException("User doesn't exist for given id: " + userId));
                .orElseThrow(() -> new UserDoesntExistException(userId.toString()));
//                .orElse(null);
        model.addAttribute("userDetails", userDetails);

        return "userDetails";
    }

    @GetMapping("/usersWE/{userId}")
    public String showUserDetailsPageWithException(@PathVariable Long userId, Model model) {

        UserDetailsDto userDetails = userService.getUserDetailsWithException(userId);
        model.addAttribute("userDetails", userDetails);

        return "userDetails";
    }
}
