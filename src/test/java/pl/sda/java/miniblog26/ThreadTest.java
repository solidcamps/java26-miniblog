package pl.sda.java.miniblog26;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class ThreadTest {

    @Test
    void simpleThread() throws InterruptedException {

        final MyThread myThread = new MyThread();
        myThread.start();
        log.info("after start...");

        final MyThread myThread2 = new MyThread();
        myThread2.start();

        myThread.join();
        myThread2.join();
        log.info("after join...");

    }

    @Test
    void service() throws InterruptedException {
        final StocksService stocksService = new StocksService();
        log.info("THE SLOW WAY");
        log.info("Price: {}", stocksService.getStockValue("CDR"));
        log.info("Price: {}", stocksService.getStockValue("PZU"));
        log.info("Price: {}", stocksService.getStockValue("PKO"));

        // napisz kod, który zwróci wszystkie 3 notowania jak najszybciej.
        log.info("THE FAST WAY");
        final MyThreadWrapper myThread1 = new MyThreadWrapper(stocksService, "PZU");
        final MyThreadWrapper myThread2 = new MyThreadWrapper(stocksService, "PKO");
        final MyThreadWrapper myThread3 = new MyThreadWrapper(stocksService, "CDR");

        final Thread thread1 = new Thread(myThread1);
        thread1.start();
        final Thread thread2 = new Thread(myThread2);
        thread2.start();
        final Thread thread3 = new Thread(myThread3);
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();
        log.info("Price: {}", myThread1.getValue());
        log.info("Price: {}", myThread2.getValue());
        log.info("Price: {}", myThread3.getValue());
//        thread1 = getValueForPZU.run
    }

}
