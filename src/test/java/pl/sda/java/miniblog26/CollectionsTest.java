package pl.sda.java.miniblog26;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

public class CollectionsTest {

    public static final List<Integer> LOOKUP_LIST =
            List.of(1, 4, 3, 2, 12, 11, 13, 97, 98, 99, 49, 50, 51, 99, 100, 98);

    @Test
    void comparingCollections() {
        final List<Integer> items = List.of(12, 99, 50, 100);
        final List<Integer> lookupList = LOOKUP_LIST;

        Set<Integer> lookupSet = new HashSet(lookupList);



        // return 1-100;

        for (Integer item : items) { //12
//            final boolean contains = lookupList.contains(item);
//            for (Integer i : lookupList) { //1, 4, 3, 2, 12, 11
//                if (item.equals(i)) {
//                    //
//                }
//            }
            lookupSet.contains(item);
        }
    }

    @Test
    void productElements() {
        final List<Integer> list1 = convert4(List.of(12, 100, 300));
        System.out.println("List1: " + list1);
    }

    private List<Integer> convert1(Collection<Integer> collection){
        Set<Integer> lookupSet = new HashSet(LOOKUP_LIST);
        return collection
                .stream()
                .map(value -> multiplyIfContained(lookupSet, value))
                .collect(Collectors.toList());
    }

    private Integer multiplyIfContained(Set<Integer> lookupSet, Integer value) {
        if (lookupSet.contains(value)) {
            return value * 2;
        } else {
            return value;
        }
    }

    private List<Integer> convert2(Collection<Integer> collection) {
//        final List<Integer> items = List.of(12, 99, 50, 100);
        Set<Integer> lookupSet = new HashSet(LOOKUP_LIST);
        List<Integer>  convertedList =  new ArrayList<>();
        for (Integer item : collection) {
            if(lookupSet.contains(item)) {
                convertedList.add(item * 2);
            } else {
                convertedList.add(item);
            }
        }
        return convertedList;
    }

    private List<Integer> convert3(Collection<Integer>collection){
//        final List<Integer> items = List.of(12, 99, 50, 100, 200);
//        final List<Integer> lookupList = List.of(1, 4, 5, 11, 12, 99, 50, 99,99100, 100, 99,66,50);
        List<Integer>  convertedList =  new ArrayList<>();
//        if (LOOKUP_LIST.containsAll(collection)){
            for (Integer intem : collection) {
                boolean contains = false;
                for (Integer i : LOOKUP_LIST) {
                    if (intem.equals(i)) {
                        contains = true;
                        continue;
                    }
//                    i.setNewValue(i*2);
//                    i = new Integer(10);
//                    i=i*2;
                }
                if (contains) {
                    convertedList.add(intem * 2);
                } else {
                    convertedList.add(intem);
                }
            }
//        }else return LOOKUP_LIST;
        return convertedList;
    }
    private List<Integer> convert4(Collection<Integer>collection){
//    final List<Integer> items = List.of(12);
//    final List<Integer> lookupList = List.of(1, 2, 3, 12);
    Set<Integer> lookupSet  = new HashSet(LOOKUP_LIST);
    List<Integer> result = new ArrayList<>();
        for (Integer item : collection){
        if (lookupSet.contains(item)){
            result.add(item*2);
        } else {
            result.add(item);
        }
    }
        return result;
}

    private List<Integer> convert5(Collection<Integer>collection){
        Collection<Integer> col = new MyCustomCollection();
        col.add(10);
        Collection<Integer> collection1 = new Collection<Integer>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Integer> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] ts) {
                return null;
            }

            @Override
            public boolean add(Integer integer) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> collection) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Integer> collection) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> collection) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> collection) {
                return false;
            }

            @Override
            public void clear() {

            }
        };
        return (List<Integer>) collection1;
    }

    private List<Integer> convert6(Collection<Integer>collection){
//        List<Integer> a = List.of(12, 15, 45, 49);
        List<Integer> equ = new ArrayList<>();
        for (Integer integer : collection) {
            if (LOOKUP_LIST.contains(integer)){
                equ.add(integer);
            }
        }
        return equ;
    }

    private List<Integer> convert7(Collection<Integer> collection){
        List<Integer> convertedList = new ArrayList<>();

        collection.stream()
                .filter(LOOKUP_LIST::contains)
                .forEach(item -> convertedList.add(item * 2));

        collection.stream()
                .filter(item -> !LOOKUP_LIST.contains(item))
                .forEach(convertedList::add);

        return convertedList;
    }

//    private ? countItems(Collection<Integer> inputCollection) {
//
//        // zwrócić w jakiejś formie strukturę, która będzie informowała o tym
//        // ile razy dany element występuje w kolekcji.
//    }

    @Test
    void testCounter() {
        final List<Integer> integers = List.of(1, 2, 3, 2, 2, 3);
        final Map<Integer, Integer> countMap = countItems3(integers);
        System.out.println("countMap: " + countMap);
    }

    private void countItems1(Collection<Integer> collection){
        List<Integer> lookupList = List.of(1, 4, 5, 4,5,11, 12, 99, 50, 99,99100, 100, 99,66,50);
        for(Integer i : collection){
            int freq = Collections.frequency(lookupList, i);
            System.out.println("Element "+i+" wystepuje "+freq+" razy");
        }
    }

    private Map<Integer, Long> countItems2(Collection<Integer> inputCollection) {
        return inputCollection.stream()
                .collect(Collectors.groupingBy(value -> value, Collectors.counting()));
    }

    private Map <Integer, Integer> countItems3(Collection<Integer> inputCollection) {
        Map<Integer, Integer> map = new HashMap<>();
//        int counter = 0;
        for ( Integer item: inputCollection) {
//            final Integer itemCounter = map.get(item);
//            if(itemCounter == null) {
//                map.put(item, 1);
//            } else {
//                map.put(item, itemCounter + 1);
//            }
            final Integer itemCounter = map.getOrDefault(item, 0);
            map.put(item, itemCounter + 1);

        }
        return map;
    }

    private Map<Integer, Integer> countItems4(Collection<Integer> inputCollection){
        Map<Integer, Integer> result = new HashMap<>();
        for (Integer i : inputCollection){
            result.put(i, result.getOrDefault(i, 0)+1);
        }
        return result;
    }

//    Collection<Integer> inputCollection = List.of(3, 4, 4, 5, 8, 2, 3);
    private int countItems5(Collection<Integer> inputCollection) {
        List<Integer> list = new ArrayList<>();
        int howMany = 1;
        int count = 0;
        for (Integer i : inputCollection){
            if(inputCollection.contains(i)){
//                howMany += howMany;
                howMany = howMany + howMany;
//                howMany += 1;
//                howMany = howMany + 1;
//                howMany++;
                list.add(i);
            }
        }
        return howMany;
    }

}
