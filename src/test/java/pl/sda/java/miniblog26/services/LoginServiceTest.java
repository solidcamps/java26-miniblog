package pl.sda.java.miniblog26.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sda.java.miniblog26.dao.UserRepository;
import pl.sda.java.miniblog26.entities.UserEntity;
import pl.sda.java.miniblog26.exceptions.InvalidCredentialsException;
import pl.sda.java.miniblog26.exceptions.UserDoesntExistException;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoginServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private LoginService loginService;

    @Test
    @DisplayName("Should login User with valid credentials")
    void should_login_user_with_valid_credentials() {
        //given
        String validUsername = "valid.user@test.pl";
        String validPassword = "VALID_PASSWORD";

        final UserEntity validUserEntity = new UserEntity();
        validUserEntity.setEmail(validUsername);
        validUserEntity.setPassword(validPassword);

        when(userRepository.findUserByLogin(validUsername))
                .thenReturn(Optional.of(validUserEntity));

        //when
        loginService.loginUser(validUsername, validPassword);

        //then
        org.junit.jupiter.api.Assertions.assertEquals(true, loginService.isLogged());
        assertThat(loginService.isLogged()).isEqualTo(true);
        assertThat(loginService.isLogged()).isTrue();
    }

    @Test
    void should_not_login_user_with_invalid_username() {
        //given
        String validUsername = "valid.user@test.pl";
        String validPassword = "VALID_PASSWORD";
        String invalidUsername = "invalid.user@test.pl";

        final UserEntity validUserEntity = new UserEntity();
        validUserEntity.setEmail(validUsername);
        validUserEntity.setPassword(validPassword);

        lenient().when(userRepository.findUserByLogin(validUsername))
                .thenReturn(Optional.of(validUserEntity));
        when(userRepository.findUserByLogin(any()))
                .thenReturn(Optional.empty());

        //when
        try {
            loginService.loginUser(invalidUsername, validPassword);
            fail("Login service should throw exception");
        } catch (UserDoesntExistException e) {
            // test ok...
        }

        //then
        assertThat(loginService.isLogged())
                .isFalse();
    }

    @Test
    void should_not_login_user_when_password_is_invalid() {
        //given
        String validUsername = "valid.user@test.pl";
        String validPassword = "VALID_PASSWORD";
        String invalidPassword = "INVALID_PASSWORD";

        final UserEntity validUser = new UserEntity();
        validUser.setEmail(validUsername);
        validUser.setPassword(validPassword);

        when(userRepository.findUserByLogin(validUsername))
                .thenReturn(Optional.of(validUser));


        //when
        //JUnit5
        Assertions.assertThrows(InvalidCredentialsException.class,
                () -> loginService.loginUser(validUsername, invalidPassword) );

        //AssertJ
        assertThatThrownBy(() -> loginService.loginUser(validUsername, invalidPassword))
                .isInstanceOf(InvalidCredentialsException.class)
                .hasMessageContaining("invalid");

        //AssertJ - different approach
        final Throwable caughtException = catchThrowable(() -> loginService.loginUser(validUsername, invalidPassword));


        //then
        assertThat(caughtException)
                .isInstanceOf(InvalidCredentialsException.class)
                .hasMessageContaining("invalid");
        assertThat(loginService.isLogged()).isFalse();
    }
}
