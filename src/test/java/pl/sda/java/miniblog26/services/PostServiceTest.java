package pl.sda.java.miniblog26.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sda.java.miniblog26.dao.PostCommentRepository;
import pl.sda.java.miniblog26.dao.PostRepository;
import pl.sda.java.miniblog26.dtos.PostInfoDto;
import pl.sda.java.miniblog26.entities.PostCommentEntity;
import pl.sda.java.miniblog26.entities.PostEntity;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
class PostServiceTest {

    @Mock
    private PostRepository postRepository;

    @Mock
    private PostCommentRepository postCommentRepository;

    @InjectMocks
    private PostService postService;

    @Test
    void should_return_post_dto_with_comments_for_existing_post() {
        //given
        Long postId = 10L;
        final PostEntity postEntity = new PostEntity();
        postEntity.setId(postId);
        Mockito.when(postRepository.findById(postId))
                .thenReturn(Optional.of(postEntity));

        Mockito.when(postCommentRepository.findByPostId(eq(postId), any()))
                .thenReturn(List.of(new PostCommentEntity(), new PostCommentEntity()));

        //when
        final Optional<PostInfoDto> postInfoWithComments =
                postService.getSinglePostInfoWithComments(postId);

        //then
        assertThat(postInfoWithComments).isNotEmpty();
        assertThat(postInfoWithComments.get())
                .hasFieldOrPropertyWithValue("id", postId);

        assertThat(postInfoWithComments.get().getComments())
                .hasSize(2);
    }

}
