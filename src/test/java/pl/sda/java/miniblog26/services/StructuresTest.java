package pl.sda.java.miniblog26.services;

import org.junit.jupiter.api.Test;
import org.springframework.data.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StructuresTest {

    @Test
    void test1() {
        // idA, idB
        // 1, 2
        // 1, 3
        // 2, 5

        int a = 1, b = 2;
        long c = (long) a;

        Pair.of(a, b);

//        (Long)((Object) (new Integer(2)))

        Long x = Long.valueOf(10);
        final Number n = x;
        System.out.println("x: " + (Long) n);

        List<Pair<Long, Long>> aTable = new ArrayList<>();
        aTable.add(Pair.of(1L, (long) 2));
        aTable.add(Pair.of(Long.valueOf(1), 3L));
        aTable.add(Pair.of(2L, 5L));

        // get all idB for idA = 1;
        Map<Long,Long> idOneMap = new HashMap<>();
        for( Pair<Long, Long> p :aTable) {
            if(p.getFirst().equals(1L)){
                idOneMap.put(p.getFirst(), p.getSecond());
            }
        }
        System.out.println("result for idA=1 :: " + idOneMap);

        final var idsB = aTable.stream()
                .filter(p -> p.getFirst().equals(1L))
                .map(Pair::getSecond)
                .collect(Collectors.toSet());

        System.out.println("isdB: " + idsB);

    }

}
