package pl.sda.java.miniblog26;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MyThreadWrapper implements Runnable {

    private final StocksService stocksService;

    private String value;
    private String symbol;

    public MyThreadWrapper(StocksService stocksService, String symbol) {
        this.symbol = symbol;
        this.stocksService = stocksService;
    }

    @Override
    public void run() {
        value = stocksService.getStockValue(symbol);
        log.info("Running, response: {}", value);
    }

    public String getValue() {
        return value;
    }
}
