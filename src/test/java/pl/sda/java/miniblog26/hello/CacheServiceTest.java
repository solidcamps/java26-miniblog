package pl.sda.java.miniblog26.hello;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CacheServiceTest {

    @Mock
    private HelloDateTimeProvider helloDateTimeProvider;

    @InjectMocks
    private CacheService<String> stringCacheService;

    @Test
    void should_return_cached_value_when_not_expired() {

        //given
        final String testString = "TEST_STRING";
        final LocalDateTime localDateTime = LocalDateTime.of(2020, 12, 6, 13, 23, 17);
        when(helloDateTimeProvider.getCurrentTime())
                .thenReturn(localDateTime)
                .thenReturn(localDateTime.plusSeconds(5));
        stringCacheService.put(testString, Duration.ofSeconds(10));

        //when
        final String stringFromCache = stringCacheService.get();

        //then
        assertThat(stringFromCache)
                .isEqualTo(testString);
    }

    @Test
    void should_return_null_when_expired() {
        //given
        final String testString = "EXPIRED_TEST_STRING";
        final LocalDateTime localDateTime = LocalDateTime.of(2020, 12, 6, 13, 23, 17);
        when(helloDateTimeProvider.getCurrentTime())
                .thenReturn(localDateTime)
                .thenReturn(localDateTime.plusSeconds(5));
        stringCacheService.put(testString, Duration.ofSeconds(3));

        //when
        final String cachedString = stringCacheService.get();

        //then
        assertThat(cachedString).isNull();
    }

}
