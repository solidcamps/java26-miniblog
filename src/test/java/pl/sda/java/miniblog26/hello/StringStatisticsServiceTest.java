package pl.sda.java.miniblog26.hello;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

class StringStatisticsServiceTest {

    private final StringStatisticsService stringStatisticsService = new StringStatisticsService();

    @Test
    void should_return_correct_sum_when_many_inputs() {
        //given
        String firstWord = "ala";
        String secondWord = "ma";
        String thirdWord = "kota";

        //when
        final int sumAllCharacters = stringStatisticsService.sumAllCharacters(firstWord, secondWord, thirdWord);

        //then
        Assertions.assertThat(sumAllCharacters)
                .isEqualTo(9);
    }

    @Test
    void should_return_correct_sum_when_there_are_null_elements() {
        //given
        String firstWord = "ala";
        String secondWord = null;
        String thirdWord = "kota";

        //when
        final int sumAllCharacters = stringStatisticsService.sumAllCharacters(firstWord, secondWord, thirdWord);

        //then
        Assertions.assertThat(sumAllCharacters)
                .isEqualTo(7);
    }

    @Test
    void should_return_true_on_even_length() {
        //given
        String firstWord = "ala";
        String secondWord = "ma kota";

        //when
        final boolean lengthEven = stringStatisticsService.isLengthEven(firstWord, secondWord);

        //then
        Assertions.assertThat(lengthEven)
                .isTrue();

    }

    @Test
    void should_return_false_on_odd_length() {
        //given
        String firstWord = "ala";
        String secondWord = "ma";

        //when
        final boolean lengthEven = stringStatisticsService.isLengthEven(firstWord, secondWord);

        //then
        Assertions.assertThat(lengthEven)
                .isFalse();

    }

    @Test
    void should_return_characters_count() {
        //given
        String firstWord = "ala";
        String secondWord = "ma";
        String thirdWord = null;

        //when
        final Map<String, Integer> charactersOccurrence =
                stringStatisticsService.countCharactersOccurrence(firstWord, secondWord, thirdWord);

        //then
        Assertions.assertThat(charactersOccurrence)
                .hasSize(3)
                .containsEntry("a", 3)
                .containsEntry("l", 1)
                .containsEntry("m", 1);

    }

}
