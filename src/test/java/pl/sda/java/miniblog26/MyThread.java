package pl.sda.java.miniblog26;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MyThread extends Thread {

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.error("err", e);
        }
        log.info("MyThread started...");
    }
}
