package pl.sda.java.miniblog26;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

public class AdvancedTest {

    @Test
    void testAdvancedCounter() {
        final List<Person> people = List.of(
                new Person("Jan", "Kowalski", 33),
                new Person("Jan", "Nowak", 25),
                new Person("Adam", "Kowalski", 25),
                new Person("Adam", "Nowak", 37),
                new Person("Marian", "Paździoch", 43)
        );

        final Map<String, Integer> peoplesAge = computeMaxAgePerFirstName2(people);
        System.out.println("peoplesAge: " + peoplesAge);
    }

    private Map<String, Integer> computeMaxAgePerFirstName1(List<Person> people){
        Map<String, Integer> result = new HashMap<>();
        for (Person p : people){
            Integer maxAge = result.getOrDefault(p.getFirstName(), 0);
            if (p.getAge() > maxAge) {
                result.put(p.getFirstName(), p.getAge());
            }
        }
        return result;
    }

    private Map<String, Integer> computeMaxAgePerFirstName2(List<Person> people) {
        Comparator<String> byLength = Comparator.comparing(String::length);
        return people.stream().collect(
                groupingBy(Person::getFirstName,
                        reducing(0,
                                Person::getAge,
                                BinaryOperator.maxBy(Comparator.naturalOrder()))));
    }

    private Map<Person,Integer> computeMaxAgePerFirstName3(List<Person> people) {
        Set<Person> peopleSet = new HashSet(people);
        Map<Person,Integer> map = new HashMap<>();
        for(Person item : people){
//            if(i)
        }
        return map;
    }

    private Map<String , Integer> computeMaxAgePerFirstName4(List<Person> people){
        Map<String , Integer> compute = new HashMap<>();
        for (Person name : people){
        }
        return null;
    }



    @Test
    void lambda() {

        final Function<String, Integer> function = new Function<>() {
            @Override
            public Integer apply(String s) {
                return s.length();
            }
        };


        System.out.println(function.apply("qwerty"));

        final List<Integer> col = List.of("xyz", "qwer")
                .stream()
                .map(function)
                .collect(Collectors.toList());
        System.out.println(col);
    }
    @Test
    void lambda2() {

        final List<Integer> col = List.of("xyz", "qwer")
                .stream()
                .map(s -> s.length())
                .collect(Collectors.toList());
        System.out.println(col);
    }
}
